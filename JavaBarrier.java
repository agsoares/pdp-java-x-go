
import java.util.Arrays;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

class JavaBarrier {
	public static void main(String[] args) {
		if (args.length < 1) {
		      System.out.println("usage: java JavaBarrier <number of rows>");
		      return;
		} else{
			int n = Integer.parseInt(args[0]);
			int[][] matrix = new int[n][n];
			Solver solver = new Solver(matrix);
		}
	}
}
class Solver {
	final int N;
	final int[][] data;
	final CyclicBarrier barrier;
	
	class Worker implements Runnable {
		int myRow;
		boolean done = false;
	    Worker(int row){   // construtor
	    	myRow = row; 
	    }
	    @Override
	    public void run() {
	    	while (!done) {
	    		processRow(myRow);
	    		try {
	    			System.out.println(Thread.currentThread().getName() + " is waiting on barrier\n");
	    			barrier.await();
	    			System.out.println(Thread.currentThread().getName() + " has crossed the barrier");
	    		} catch (InterruptedException ex) { 
	    			return; 
	    		} catch (BrokenBarrierException ex) { 
	    			return; 
	    		}
	    	}
	    	//System.out.println("Run finish for " + Thread.currentThread().getName());
	    }
	    private void processRow(int myRow) {
	    	int[] row = data[myRow];
	    	System.out.print(Thread.currentThread().getName() + " is processing a line \n");
            for (int i = 0; i < row.length; i++) {
                row[i] = (int) (Thread.currentThread().getId()-i);
            }
            done = true;
            System.out.println();
	    }
   }
   public Solver(int[][] matrix) {  // construtor
	   data = matrix;
	   N = matrix.length;
	   barrier = new CyclicBarrier(N, new Runnable(){
		   public void run(){ 
			   mergeRows(); // executa quando todas threads chegaram na barreira
		   }
		   private void mergeRows() {
			   System.out.println("Matrix merged by thread: " + Thread.currentThread().getName());
			   for (int i = 0; i < data.length; i++) {
                   System.out.println(Arrays.toString(data[i]));
               }
		   }
	   });
	   for (int i = 0; i < N; ++i)
		   new Thread(new Worker(i), "thread "+ i).start();
   }
}
