package main

import (
  "os"
  "fmt"
  "time"
  "strconv"
  "math/rand"
)
var c chan int = make(chan int, 10)

func producer(id int) {
  for {
    time.Sleep((time.Second*time.Duration(rand.Intn(5)+1)))
    msg := rand.Intn(100)
    c <- msg
    fmt.Println(strconv.Itoa(id) + " Produced " + strconv.Itoa(msg))
  }
}

func consumer(id int) {
  for {
    msg := <- c
    fmt.Println(strconv.Itoa(id) + " Consumed " + strconv.Itoa(msg))
    time.Sleep((time.Second*time.Duration(rand.Intn(5)+14)))
  }
}

func main() {
  if len(os.Args) < 3 {
    fmt.Println("usage: GoProducerConsumer <number of consumers> <number of producers>")
  } else {
    rand.Seed(time.Now().UTC().UnixNano())
    consumers, _:= strconv.Atoi(os.Args[1])
    for i := 0; i < consumers; i++ { 
      go consumer(i)
    }

    producers, _:= strconv.Atoi(os.Args[2])
    for i := 0; i < producers; i++ { 
      go producer(i)
    }

    var input string
    fmt.Scanln(&input)
  }
}