import java.lang.*;
import java.util.*;
import java.util.concurrent.Semaphore;

class JavaReadersWriters {
  public static void main(String args[]){
    int i;
    if (args.length < 2) {
      System.out.println("usage: java JavaReadersWriters <number of readers> <number of writers>");
      return;
    }
    Resource resource = new Resource();
    int readers = Integer.parseInt(args[0]);
    int writers = Integer.parseInt(args[1]);
    for (i = 0; i < readers; i++) {
      Reader r = new Reader(i, resource);
      r.start();
    }
    for (i = 0; i < writers; i++) {
      Writer w = new Writer(i, resource);
      w.start();
    }
    Scanner scanner = new Scanner(System.in);
    scanner.next();
  }
}

class Resource {

  private int readers       = 0;
  private int writers       = 0;
  private int writeRequests = 0;

  public synchronized void lockRead() throws InterruptedException{
    while(writers > 0 || writeRequests > 0){
      wait();
    }
    readers++;
  }

  public synchronized void unlockRead(){
    readers--;
    notifyAll();
  }

  public synchronized void lockWrite() throws InterruptedException{
    writeRequests++;

    while(readers > 0 || writers > 0){
      wait();
    }
    writeRequests--;
    writers++;
  }

  public synchronized void unlockWrite() {
    writers--;
    notifyAll();
  }
  
  public Resource() {
  }
}

class Writer extends Thread {
  int id;
  Resource r;
  public Writer(int id, Resource resource) {
    this.id = id;
    r = resource;
  }
  
  @Override
  public void run() {
    Random rand = new Random();
      
    while (true) {
      try {
        Thread.sleep(1000*rand.nextInt(5)+1); 
        System.out.println("Writer " + id + " wants to write");
        r.lockWrite();
      } catch(InterruptedException ex) {
        Thread.currentThread().interrupt();
      } 

      System.out.println("Writer " + id + " is writing");
      
      try {
        Thread.sleep(1000*rand.nextInt(5)+1);
      } catch(InterruptedException ex) {
        Thread.currentThread().interrupt();
      } 
      r.unlockWrite();
      System.out.println("Writer " + id + " finished writing");
    }
  }
}

class Reader extends Thread {
  int id;
  Resource r;
  public Reader(int id, Resource resource) {
    this.id = id;
    r = resource;
  }
  
  @Override
  public void run() {
    Random rand = new Random(); 
    while (true) {
      try {
        Thread.sleep(1000*rand.nextInt(5)+1); 
        System.out.println("Reader " + id + " wants to read");
        r.lockRead();
      } catch(InterruptedException ex) {
        Thread.currentThread().interrupt();
      } 

      System.out.println("Reader " + id + " is reading");
      
      try {
        Thread.sleep(1000*rand.nextInt(5)+1);
      } catch(InterruptedException ex) {
        Thread.currentThread().interrupt();
      } 
      r.unlockRead();
      System.out.println("Reader " + id + " finished reading");
    }
  }
}