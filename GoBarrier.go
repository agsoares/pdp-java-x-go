package main

import (
	"fmt"
	"os"
	"sync"
	"math/rand"
	"time"
	"strconv"
)

var N int
var barrier sync.WaitGroup
var matrix [][]int
var matrix_lin int

func init_matrix (matrix *[][]int, lin int) {
	fmt.Printf("Matrix:\n")
	for i := 0; i < lin; i++ {
		for j := 0; j < lin; j++ {
			(*matrix)[i][j] = rand.Intn(11);
			fmt.Printf(" %d", (*matrix)[i][j])
		}
	fmt.Printf("\n")
	}

}

func Worker (lin int){
	myrow := make([]int, len(matrix))
    time.Sleep((time.Second*time.Duration(rand.Intn(5)+1)))
	fmt.Printf("Sou a goroutine %d! Processo essa linha: ", lin)
	for i:=0 ; i<len(matrix) ; i++ {
		myrow[i] = matrix[lin][i]
		fmt.Printf("%d ", myrow[i])
	}
	barrier.Done()
	fmt.Printf("\n")
}

func Solver (n int){
	for i := 0; i < n; i++ {

		barrier.Add(1)
		go Worker(i)
	}
	barrier.Wait()
	fmt.Printf("Barreira ultrapassada por todos!")
}

func main(){
	if len(os.Args) < 2 {
    		fmt.Println("usage: GoBarrier <number of rows>")
  	} else {
        rand.Seed(time.Now().UTC().UnixNano())
		N,_ = strconv.Atoi(os.Args[1])
		matrix_lin = N
		matrix = make([][]int, matrix_lin)
		for i := 0; i < matrix_lin; i++ {
			matrix[i] = make([]int, matrix_lin)
		}
		init_matrix(&matrix, matrix_lin)
		Solver(N)
		fmt.Printf("\n")
	}
}