package main

import (
  "os"
  "fmt"
  "time"
  "strconv"
  "math/rand"
  "sync"
)

var mutex sync.RWMutex

func reader(id int) {
  for {
    time.Sleep((time.Second*time.Duration(rand.Intn(5)+1)))
    fmt.Println("Reader " + strconv.Itoa(id) + " wants to read")
    mutex.RLock();
    
    
    fmt.Println("Reader " + strconv.Itoa(id) + " is reading")
    time.Sleep((time.Second*time.Duration(rand.Intn(10)+1)))

    mutex.RUnlock();
    fmt.Println("Reader " + strconv.Itoa(id) + " finished reading")
    
  }
}

func writer(id int) {
  for {
    time.Sleep((time.Second*time.Duration(rand.Intn(5)+1)))
    fmt.Println("Writer " + strconv.Itoa(id) + " wants to write")
    mutex.Lock();
    
    
    fmt.Println("Writer " + strconv.Itoa(id) + " is writing")
    time.Sleep((time.Second*time.Duration(rand.Intn(10)+1)))

    mutex.Unlock();
    fmt.Println("Writer " + strconv.Itoa(id) + " finished writing")
  }
}

func main() {
  if len(os.Args) < 3 {
    fmt.Println("usage: GoReadersWriters <number of readers> <number of writers>")
  } else {
    rand.Seed(time.Now().UTC().UnixNano())
    readers, _:= strconv.Atoi(os.Args[1])
    for i := 0; i < readers; i++ { 
      go reader(i)
    }

    writers, _:= strconv.Atoi(os.Args[2])
    for i := 0; i < writers; i++ { 
      go writer(i)
    }

    var input string
    fmt.Scanln(&input)
  }
}