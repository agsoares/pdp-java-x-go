import java.lang.*;
import java.util.*;
import java.util.concurrent.Semaphore;

class JavaProducerConsumer {
  public static void main(String args[]){
    int i;
    if (args.length < 2) {
      System.out.println("usage: java JavaProducerConsumer <number of consumers> <number of producers>");
      return;
    }
    Buffer buffer = new Buffer(10);
    int consumers = Integer.parseInt(args[0]);
    int producers = Integer.parseInt(args[1]);
    for (i = 0; i < consumers; i++) {
      Consumer c = new Consumer(i, buffer);
      c.start();
    }
    for (i = 0; i < producers; i++) {
      Producer p = new Producer(i, buffer);
      p.start();
    }
    Scanner scanner = new Scanner(System.in);
    scanner.next();
  }
}

class Buffer {
  Semaphore empty;
  Semaphore full;
  LinkedList<Integer> list;
  public Buffer(int capacity) {
    list = new LinkedList<Integer>();
    empty = new Semaphore(0);
    full = new Semaphore(capacity);
  }
}

class Producer extends Thread {
  int id;
  Buffer buffer;
  public Producer(int id, Buffer b) {
    this.buffer = b;
    this.id = id;
  }

  @Override
  public void run() {
    Random rand = new Random();
    while (true) {
      try {
        buffer.full.acquire();
      } catch (Exception e) { }

      try {
        Thread.sleep(1000*rand.nextInt(5)+1);
      } catch(InterruptedException ex) {
        Thread.currentThread().interrupt();
      }
      synchronized (buffer) {
        int msg = rand.nextInt(100);
        buffer.list.add(new Integer(msg));
        System.out.println(id + " Produced " + msg);
      }

      try {
        buffer.empty.release();
      } catch (Exception e) { }
    }
  }
}

class Consumer extends Thread {
  int id;
  Buffer buffer;
  public Consumer (int id, Buffer b) {
    this.buffer = b;
    this.id = id;
  }

  @Override
  public void run() {
    Random rand = new Random();
    while (true) {
      try {
        buffer.empty.acquire();
      } catch (Exception e) { }

      synchronized (buffer) {
        int msg = ((Integer)buffer.list.remove());
        System.out.println(id + " Consumed " + msg);
      }
      try {
        Thread.sleep(1000*rand.nextInt(5)+1);
      } catch(InterruptedException ex) {
        Thread.currentThread().interrupt();
      }

      try {
        buffer.full.release();
      } catch (Exception e) { }
    }
  }
}
